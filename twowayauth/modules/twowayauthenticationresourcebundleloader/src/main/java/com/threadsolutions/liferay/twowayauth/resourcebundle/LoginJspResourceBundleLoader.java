package com.threadsolutions.liferay.twowayauth.resourcebundle;

import com.liferay.portal.kernel.util.AggregateResourceBundleLoader;
import com.liferay.portal.kernel.util.CacheResourceBundleLoader;
import com.liferay.portal.kernel.util.ClassResourceBundleLoader;
import com.liferay.portal.kernel.util.ResourceBundleLoader;

import java.util.Locale;
import java.util.ResourceBundle;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
	immediate = true,
	property = {
		"bundle.symbolic.name=com.liferay.login.web",
		"resource.bundle.base.name=content.Language",
		"servlet.context.name=login-web"
	}
)
public class LoginJspResourceBundleLoader implements ResourceBundleLoader {

	private AggregateResourceBundleLoader _resourceBundleLoader;

	@Override
	public ResourceBundle loadResourceBundle(Locale locale) {
		return _resourceBundleLoader.loadResourceBundle(locale);
	}

	@Override
	public ResourceBundle loadResourceBundle(String languageId) {
		return _resourceBundleLoader.loadResourceBundle(languageId);
	}

	@Reference(target = "(&(bundle.symbolic.name=com.liferay.login.web)(!(component.name=com.threadsolutions.liferay.twowayauth.resourcebundle.LoginJspResourceBundleLoader)))")
	public void setResourceBundleLoader(ResourceBundleLoader resourceBundleLoader) {
		_resourceBundleLoader = new AggregateResourceBundleLoader(
			new CacheResourceBundleLoader(
				new ClassResourceBundleLoader(
					"content.Language",
					LoginJspResourceBundleLoader.class.getClassLoader()
				)
			),
			resourceBundleLoader
		);
	}

}
