# TwoWayAuthenticationResourceBundle

Checkout repository

Open Liferay Developer Studio

Import Liferay Module Project(s)

Select ```twowayauthenticationresourcebundle/twowayauthentication``` location

Import ```twowayauthenticationresourcebundleloader``` module

Modify language file or add new file

Build module and deploy the jar file

Restart Liferay
